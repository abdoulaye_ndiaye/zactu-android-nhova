package com.app.mobile.zactu.model;

/**
 * Created by ghambyte on 01/12/2017 at 09:59.
 * Project name : BGFIMobile
 */

public class Client {

    String nom;
    String email;
    String date;

    public Client() {
    }

    public Client(String nom, String email, String date) {
        this.nom = nom;
        this.email = email;
        this.date = date;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Client{" +
                "nom='" + nom + '\'' +
                ", email='" + email + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
