package com.app.mobile.zactu;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.mobile.zactu.model.Client;
import com.app.mobile.zactu.model.News;
import com.app.mobile.zactu.model.ResponseBase;
import com.app.mobile.zactu.model.User;
import com.app.mobile.zactu.rest.UserService;
import com.app.mobile.zactu.tools.Utilities;
import com.app.mobile.zactu.viewholder.PostViewHolder;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Executor;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityFavori extends AppCompatActivity {


    private static final int RC_SIGN_IN = 9001;
    private static final String TAG = "ActivityFavori";
    private DatabaseReference commentRef;
    private FirebaseRecyclerAdapter<News, PostViewHolder> mAdapter;
    private RecyclerView mRecycler;
    InterstitialAd mInterstitialAd;
    private ProgressDialog mProgressDialog;
    private FirebaseAuth mAuth;
    private ProgressBar progressbar;
    TextView text_user_unconnected;
    private LinearLayout signInGoogleButton;
    private Toolbar toolbar;
    private ActionBar actionBar;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private GoogleApiClient mGoogleApiClient;

    String thisUserId;

    Dialog dialog = null;

    private CallbackManager callbackManager;
    UserService userService = BaseApp.mobileAdapter.create(UserService.class);

    @Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        text_user_unconnected = (TextView) findViewById(R.id.text_user_unconnected);
        initToolbar();
        mAuth = FirebaseAuth.getInstance();
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-6162942508854673/6537870546");
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
            }
        });
        requestNewInterstitial();
        mRecycler = (RecyclerView) findViewById(R.id.recyclerView);
        mRecycler.setHasFixedSize(true);
        text_user_unconnected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getUid() == null){
                    dialogPeopoleDetails();
                }
            }
        });

        final Query postsQuery = getQuery();
        if (getUid() == null){
            progressbar.setVisibility(View.GONE);
            text_user_unconnected.setVisibility(View.VISIBLE);
            text_user_unconnected.setText(R.string.str_empty_favoris);
        }else if (postsQuery != null){
            initViews();
        }
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle("FAVORIS");
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("SEE_YOUR_LOGCAT_TO_GET_YOUR_DEVICE_ID")
                .build();
        mInterstitialAd.loadAd(adRequest);
    }

    private void onStarClicked(final String id) {
        final DatabaseReference favoriteRef = FirebaseDatabase.getInstance().getReference("favorit").child(getUid()).child(id);
        favoriteRef.removeValue();
        final DatabaseReference postRef = FirebaseDatabase.getInstance().getReference("models").child(id);
        postRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                final News p = mutableData.getValue(News.class);
                if (p == null) {
                    return Transaction.success(mutableData);
                }
                if (p.stars.containsKey(getUid())) {
                    p.starCount = p.starCount - 1;
                    p.stars.remove(getUid());
                }
                mutableData.setValue(p);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b,
                                   DataSnapshot dataSnapshot) {
                Log.d(TAG, "postTransaction:onComplete:" + databaseError);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mAdapter != null) {
            mAdapter.cleanup();
        }
    }

    public String getUid() {
        if (FirebaseAuth.getInstance().getCurrentUser() == null){
            return null;
        }else {
            return FirebaseAuth.getInstance().getCurrentUser().getUid();
        }
    }

    public Query getQuery() {
        if (getUid() != null){
            Query myTopPostsQuery = FirebaseDatabase.getInstance().getReference().child("favorit").child(getUid()).child("posts");
            return myTopPostsQuery;
        }else {
            return null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-6162942508854673/6537870546");
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
            }
        });
        requestNewInterstitial();
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("Un instant...");
        }
        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void dialogPeopoleDetails() {
        dialog = new Dialog(this);
        ProgressBar progressBar;
        LoginButton loginButton;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_login);
        mAuth = FirebaseAuth.getInstance();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(this.getResources().getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        progressBar = (ProgressBar) dialog.findViewById(R.id.progressbar);
        loginButton = (LoginButton) dialog.findViewById(R.id.button_facebook_login);
        signInGoogleButton = (LinearLayout) dialog.findViewById(R.id.btn_compte_google);
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    if (user.getDisplayName() != null){
                        thisUserId = user.getUid();
                        onAuthSuccessKnownUser(user);
                    }
                } else {
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };

        callbackManager = CallbackManager.Factory.create();
        loginButton.setReadPermissions("email", "public_profile");
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                signInWithFacebook(loginResult.getAccessToken());
            }

            @Override

            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        dialog.show();
        dialog.getWindow().setAttributes(lp);
        signInGoogleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signInGoogle();
            }
        });
    }

    private void signInGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        (this).startActivityForResult(signInIntent, RC_SIGN_IN);
        dialog.dismiss();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date date = new Date();
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
                Log.d("SIGN IN GOOGLE OK :", account.getDisplayName());
                Client utilisateurC = new Client(account.getDisplayName(), account.getEmail(), dateFormat.format(date));
                saveClient(utilisateurC);
                Toast.makeText(getApplicationContext(), "Connexion réussie!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), "Echec Connexion! Veuillez ressayer plus tard.", Toast.LENGTH_SHORT).show();
                Log.d("SIGN IN GOOGLE OK :", "FAILED");
            }
        }
    }

    private void firebaseAuthWithGoogle(final GoogleSignInAccount acct) {
        showProgressDialog();
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        BaseApp.authenticatedUser = new User(acct.getDisplayName(), acct.getEmail(), acct.getFamilyName(), acct.getServerAuthCode());
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());
                        final User userG = new User(acct.getDisplayName().toLowerCase(), acct.getEmail(), acct.getPhotoUrl().toString(), FirebaseInstanceId.getInstance().getToken());
                        if (thisUserId != null){
                            Toast.makeText(getApplicationContext(), "Connected!",
                                    Toast.LENGTH_SHORT).show();
                            BaseApp.database.getReference("users").child(thisUserId).addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    User user1 = dataSnapshot.getValue(User.class);
                                    if (user1 == null){
                                        BaseApp.database.getReference("users").child(thisUserId).setValue(userG);
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        }
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                            Toast.makeText(getApplicationContext(), "Echec connexion.",
                                    Toast.LENGTH_SHORT).show();
                        }
                        hideProgressDialog();
                    }
                });
    }

    private void saveClient(Client utilisateur) {
        if (!Utilities.isInternetAvailable(getApplicationContext())) {
            progressbar.setVisibility(View.GONE);
            Utilities.showMessage(null, getString(R.string.no_internet), getApplicationContext());
            return;
        }
        progressbar.setVisibility(View.VISIBLE);
        Call<ResponseBase> operations;
        operations = userService.postUser(utilisateur);
        operations.enqueue(callbackUser);
    }

    private Callback<ResponseBase> callbackUser = new Callback<ResponseBase>() {

        @Override
        public void onResponse(Call<ResponseBase> call, Response<ResponseBase> res) {
            int code = res.code();
            if (code == 200) {
                finish();
            } else if (code == 401) {
                ResponseBody base = res.errorBody();
                if (base != null) {
                    Toast.makeText(getApplicationContext(), base.toString(), Toast.LENGTH_SHORT).show();
                }

            } else if (code == 500) {
                Toast.makeText(getApplicationContext(), R.string.str_unknown_error, Toast.LENGTH_SHORT)
                        .show();
            } else {
                Toast.makeText(getApplicationContext(), R.string.str_service_unavailable, Toast.LENGTH_SHORT)
                        .show();
            }
            progressbar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<ResponseBase> call, Throwable t) {
            progressbar.setVisibility(View.GONE);
            Toast.makeText(getApplicationContext(), R.string.str_service_unavailable, Toast.LENGTH_SHORT)
                    .show();
        }
    };

    private void signInWithFacebook(AccessToken token) {
        Log.d(TAG, "signInWithFacebook:" + token);
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener((Executor) this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                            Toast.makeText(getApplicationContext(), "Connexion réussie!", Toast.LENGTH_SHORT).show();                        }else{
                            String uid=task.getResult().getUser().getUid();
                            String name=task.getResult().getUser().getDisplayName();
                            String email=task.getResult().getUser().getEmail();
                            String image=task.getResult().getUser().getPhotoUrl().toString();
                            final User userF = new User(name.toLowerCase(), email, image, FirebaseInstanceId.getInstance().getToken());
                            BaseApp.authenticatedUser = userF;
                        }
                    }
                });
    }

    private void onAuthSuccessKnownUser(FirebaseUser user) {
        dialog.hide();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    public void initViews(){
        final Query postsQuery = getQuery();
        mAdapter = new FirebaseRecyclerAdapter<News, PostViewHolder>(News.class, R.layout.item_favorit_list_feed, PostViewHolder.class, postsQuery) {

            @Override
            protected void populateViewHolder(final PostViewHolder viewHolder,
                                              final News model,
                                              final int position) {
                viewHolder.bindToPost(model, new View.OnClickListener() {
                    @Override
                    public void onClick(View starView) {

                    }
                }, new View.OnClickListener() {
                    @Override
                    public void onClick(View starView) {

                    }
                }, new View.OnClickListener() {
                    @Override
                    public void onClick(View starView) {
                        onStarClicked(model.getId());
                    }
                }, new View.OnClickListener(){
                    @Override
                    public void onClick(View starView) {

                    }
                });
            }
        };
        mRecycler.setHasFixedSize(true);
        mRecycler.setLayoutManager(new LinearLayoutManager(this));
        mRecycler.setAdapter(mAdapter);
        progressbar.setVisibility(View.GONE);
    }
}
