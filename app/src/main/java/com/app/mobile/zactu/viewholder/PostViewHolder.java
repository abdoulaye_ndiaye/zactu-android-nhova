package com.app.mobile.zactu.viewholder;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.mobile.zactu.ActivityNewsDetails;
import com.app.mobile.zactu.R;
import com.app.mobile.zactu.adapter.AdapterNewsList;
import com.app.mobile.zactu.model.News;
import com.balysv.materialripple.MaterialRippleLayout;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;


public class PostViewHolder extends RecyclerView.ViewHolder {

    public TextView text_name;
    public TextView text_other_models;
    public TextView bt_more;
    public MaterialRippleLayout starView;
    public MaterialRippleLayout bttocomment;
    public LinearLayout linearOptions;
    public LinearLayout linearUsername;
    public TextView text_content;
    public TextView numStarsView;
    public ImageView photo_content;
    private AdapterNewsList.OnItemClickListener mOnItemClickListener;
    Context context = null;

    public PostViewHolder(View v) {
        super(v);

        linearOptions = (LinearLayout) v.findViewById(R.id.linearOptions);
        linearUsername = (LinearLayout) v.findViewById(R.id.linearUsername);
        text_name = (TextView) v.findViewById(R.id.text_name);
        text_other_models = (TextView) v.findViewById(R.id.text_other_models);
        photo_content = (ImageView) v.findViewById(R.id.photo_content);
        starView = (MaterialRippleLayout) itemView.findViewById(R.id.bt_like);
        bttocomment = (MaterialRippleLayout) itemView.findViewById(R.id.bt_to_comment);
        text_content = (TextView) v.findViewById(R.id.text_content);
    }

    public void bindToPost(final News post, View.OnClickListener starClickListener,
                           View.OnClickListener userClickListener, View.OnClickListener commentClickListener,
                           View.OnClickListener photoClickListener) {
        linearOptions.setVisibility(View.GONE);
        if (post != null){
            if (post.getUrls().get(0) != null){
                Glide.with(photo_content.getContext()).load(post.getUrls().get(0).getUrl() + "")
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .override(1480, 1000)
                        .into(photo_content);
            }
        }
        text_name.setText(post.getCategory());
        photo_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(text_content.getContext(), ActivityNewsDetails.class);
                intent.putExtra(ActivityNewsDetails.EXTRA_OBJC, post);
                text_content.getContext().startActivity(intent);
            }
        });
        text_other_models.setText(post.getPosttitre());
//        linearUsername.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(context, ActivityUserDetails.class);
//                intent.putExtra(EXTRA_OBJCT_USER, post);
//                context.startActivity(intent);
//            }
//        });
    }
}
