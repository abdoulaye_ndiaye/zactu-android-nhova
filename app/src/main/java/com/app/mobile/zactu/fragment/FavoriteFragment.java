package com.app.mobile.zactu.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.mobile.zactu.ActivityMain;
import com.app.mobile.zactu.BaseApp;
import com.app.mobile.zactu.R;
import com.app.mobile.zactu.model.News;
import com.app.mobile.zactu.model.User;
import com.app.mobile.zactu.viewholder.PostViewHolder;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.Random;
import java.util.concurrent.Executor;

/**
 * Created by ghambyte on 27/04/2017 at 15:10.
 * Project name : Modelic-V2
 */

public class FavoriteFragment extends Fragment {

    public FavoriteFragment() {}

    private static final int RC_SIGN_IN = 9001;
    private static final String TAG = "MyTopPostsFragment";
    private DatabaseReference commentRef;
    private FirebaseRecyclerAdapter<News, PostViewHolder> mAdapter;
    private RecyclerView mRecycler;
    private LinearLayoutManager mManager;
    ProgressBar mProgressbar;
    String telUser;
    InterstitialAd mInterstitialAd;
    private ProgressDialog mProgressDialog;
    int randomNumber;
    Random random = new Random();
    private FirebaseAuth mAuth;
    SwipeRefreshLayout swipeRefreshLayout;
    private ProgressBar progressbar;
    TextView text_user_unconnected;
    private LinearLayout signInGoogleButton;

    private ImageView mPasserField;

    private FirebaseAuth.AuthStateListener mAuthListener;

    private GoogleApiClient mGoogleApiClient;

    String thisUserId;

    Dialog dialog = null;

    private CallbackManager callbackManager;
    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_saved, container, false);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        progressbar = (ProgressBar) rootView.findViewById(R.id.progressbar);
        text_user_unconnected = (TextView) rootView.findViewById(R.id.text_user_unconnected);
        mAuth = FirebaseAuth.getInstance();
        mInterstitialAd = new InterstitialAd(getContext());
        mInterstitialAd.setAdUnitId("ca-app-pub-6162942508854673/6537870546");
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
            }
        });
        requestNewInterstitial();
        setHasOptionsMenu(true);
        mRecycler = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        mRecycler.setHasFixedSize(true);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mAdapter.notifyDataSetChanged();
                refreshItems();
            }
        });

        text_user_unconnected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getUid() == null){
                    dialogPeopoleDetails();
                }
            }
        });

        return rootView;
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("SEE_YOUR_LOGCAT_TO_GET_YOUR_DEVICE_ID")
                .build();
        mInterstitialAd.loadAd(adRequest);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Set up Layout Manager, reverse layout
        mManager = new LinearLayoutManager(getActivity());
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        mRecycler.setLayoutManager(mManager);
        final Query postsQuery = getQuery();
        if (getUid() == null){
            progressbar.setVisibility(View.GONE);
            text_user_unconnected.setVisibility(View.VISIBLE);
            text_user_unconnected.setText(R.string.str_empty_favoris);
        }else if (postsQuery != null){
            mAdapter = new FirebaseRecyclerAdapter<News, PostViewHolder>(News.class, R.layout.item_favorit_list_feed, PostViewHolder.class, postsQuery) {

                @Override
                protected void populateViewHolder(final PostViewHolder viewHolder,
                                                  final News model,
                                                  final int position) {
                    final DatabaseReference postRef = getRef(position);
                    commentRef = FirebaseDatabase.getInstance().getReference("comments");
                    commentRef.child(postRef.getKey()).child("numcomments").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
//                            if (dataSnapshot.getValue() == null){
//                                viewHolder.commentNumView.setText("0");
//                            }else {
//                                viewHolder.commentNumView.setText(dataSnapshot.getValue().toString());
//                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
//
                    viewHolder.bindToPost(model, new View.OnClickListener() {
                        @Override
                        public void onClick(View starView) {

                        }
                    }, new View.OnClickListener() {
                        @Override
                        public void onClick(View starView) {

                        }
                    },new View.OnClickListener() {
                        @Override
                        public void onClick(View starView) {
                            onStarClicked(model.getId());
                        }
                    }, new View.OnClickListener(){
                        @Override
                        public void onClick(View starView) {

                        }
                    });
                }
            };
            mRecycler.setAdapter(mAdapter);
            progressbar.setVisibility(View.GONE);
        }
    }

    void refreshItems() {
        onItemsLoadComplete();
    }

    void onItemsLoadComplete() {
        swipeRefreshLayout.setRefreshing(false);
    }

    private void onStarClicked(final String id) {
        final DatabaseReference favoriteRef = FirebaseDatabase.getInstance().getReference("favorit").child(getUid()).child(id);
        favoriteRef.removeValue();
        final DatabaseReference postRef = FirebaseDatabase.getInstance().getReference("models").child(id);
        postRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                final News p = mutableData.getValue(News.class);
                if (p == null) {
                    return Transaction.success(mutableData);
                }
                if (p.stars.containsKey(getUid())) {
                    p.starCount = p.starCount - 1;
                    p.stars.remove(getUid());
                }
                mutableData.setValue(p);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b,
                                   DataSnapshot dataSnapshot) {
                Log.d(TAG, "postTransaction:onComplete:" + databaseError);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mAdapter != null) {
            mAdapter.cleanup();
        }
    }

    public String getUid() {
        if (FirebaseAuth.getInstance().getCurrentUser() == null){
            return null;
        }else {
            return FirebaseAuth.getInstance().getCurrentUser().getUid();
        }
    }

    public Query getQuery() {
        if (getUid() != null){
            Query myTopPostsQuery = FirebaseDatabase.getInstance().getReference().child("favorit").child(getUid());
            return myTopPostsQuery;
        }else {
            return null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        mProgressbar.setVisibility(View.VISIBLE);
        if (!mRecycler.hasPendingAdapterUpdates()){
//            mProgressbar.setVisibility(View.GONE);
        }

        mInterstitialAd = new InterstitialAd(getContext());
        mInterstitialAd.setAdUnitId("ca-app-pub-6162942508854673/6537870546");
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
            }
        });

        requestNewInterstitial();
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getContext());
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("Un instant...");
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void dialogPeopoleDetails() {
        dialog = new Dialog(getContext());
        ProgressBar progressBar;
        LoginButton loginButton;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_login);
        mAuth = FirebaseAuth.getInstance();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getContext().getResources().getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        progressBar = (ProgressBar) dialog.findViewById(R.id.progressbar);
        loginButton = (LoginButton) dialog.findViewById(R.id.button_facebook_login);
//        mEmailField = (EditText) dialog.findViewById(R.id.field_email);
//        mPasswordField = (EditText) dialog.findViewById(R.id.field_password);
        mPasserField = (ImageView) dialog.findViewById(R.id.closeLogin);
//        mSignInButton = (Button) dialog.findViewById(R.id.button_sign_in);
//        mSignUpButton = (Button) dialog.findViewById(R.id.button_sign_up);
        signInGoogleButton = (LinearLayout) dialog.findViewById(R.id.btn_compte_google);
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    if (user.getDisplayName() != null){
                        thisUserId = user.getUid();
                        onAuthSuccessKnownUser(user);
                    }
                } else {
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };

        callbackManager = CallbackManager.Factory.create();
        loginButton.setReadPermissions("email", "public_profile");
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                signInWithFacebook(loginResult.getAccessToken());
            }

            @Override

            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        dialog.show();
        dialog.getWindow().setAttributes(lp);
//        mSignInButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                signIn();
//            }
//        });
//        mSignUpButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                signUp();
//            }
//        });
        signInGoogleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signInGoogle();
            }
        });
    }

    private void signInGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        ((ActivityMain) getContext()).startActivityForResult(signInIntent, RC_SIGN_IN);
        dialog.dismiss();
    }

    private void signInWithFacebook(AccessToken token) {
        Log.d(TAG, "signInWithFacebook:" + token);
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener((Executor) this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                            Toast.makeText(getContext(), "Echec connexion.",
                                    Toast.LENGTH_SHORT).show();
                        }else{
                            String uid=task.getResult().getUser().getUid();
                            String name=task.getResult().getUser().getDisplayName();
                            String email=task.getResult().getUser().getEmail();
                            String image=task.getResult().getUser().getPhotoUrl().toString();
                            final User userF = new User(name.toLowerCase(), email, image, FirebaseInstanceId.getInstance().getToken());
                            BaseApp.authenticatedUser = userF;
                        }
                    }
                });
    }

    private void onAuthSuccessKnownUser(FirebaseUser user) {
        dialog.hide();
//        BaseActivity.username = user.getDisplayName();
//        if (mInterstitialAd.isLoaded()) {
//            mInterstitialAd.show();
//            startActivity(new Intent(LoginActivity.this, ActivityMain.class));
//            finish();
//        } else {
//            startActivity(new Intent(LoginActivity.this, ActivityMain.class));
//            finish();
//        }
    }


}
