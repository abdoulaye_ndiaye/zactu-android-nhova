package com.app.mobile.zactu;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.IdRes;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.app.mobile.zactu.adapter.PageFragmentAdapter;
import com.app.mobile.zactu.controller.App;
import com.app.mobile.zactu.data.Tools;
import com.app.mobile.zactu.fragment.FragmentModeBeaute;
import com.app.mobile.zactu.fragment.FragmentActuPeople;
import com.app.mobile.zactu.fragment.FragmentTeleRealite;
import com.app.mobile.zactu.fragment.FragmentStars;
import com.app.mobile.zactu.fragment.FragmentVille;
import com.app.mobile.zactu.tools.MyNotificationOpenedHandler;
import com.app.mobile.zactu.tools.MyNotificationReceivedHandler;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.auth.FirebaseAuth;
import com.onesignal.OneSignal;

public class ActivityMain extends BaseActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private ActionBar actionbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private View parent_view;
    private CallbackManager callbackManager;
    private FirebaseAuth mAuth;
    private PageFragmentAdapter adapter;

    private FragmentStars f_stars;
    private FragmentTeleRealite f_telerealite;
    private FragmentModeBeaute f_modebeaute;
    private FragmentActuPeople f_actupeople;
    private AdView mAdView;
    InterstitialAd mInterstitialAd;
    public static Activity mActivity;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mActivity = this;
        FacebookSdk.sdkInitialize(getApplicationContext());
        OneSignal.startInit(this)
                .setNotificationOpenedHandler(new MyNotificationOpenedHandler())
                .setNotificationReceivedHandler(new MyNotificationReceivedHandler())
                .init();
        MobileAds.initialize(getApplicationContext(), "ca-app-pub-6162942508854673/4360120147");
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-6162942508854673/6537870546");
        requestNewInterstitial();
        setContentView(R.layout.activity_main);
        parent_view = findViewById(android.R.id.content);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(false);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        viewPager.setOffscreenPageLimit(2);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        callbackManager = CallbackManager.Factory.create();
        setupTabIcons();
        setupTabClick();
        initDrawerMenu();
//        global = (GlobalVariable) getApplication();

        // for system bar in lollipop
        Tools.systemBarLolipop(this);

        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();

        mInterstitialAd.loadAd(adRequest);
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new PageFragmentAdapter(getSupportFragmentManager());
        if (f_actupeople == null) {
            f_actupeople = new FragmentActuPeople();
        }
        if (f_stars == null) {
            f_stars = new FragmentStars();
        }
        if (f_telerealite == null) {
            f_telerealite = new FragmentTeleRealite();
        }
        if (f_modebeaute == null) {
            f_modebeaute = new FragmentModeBeaute();
        }
        adapter.addFragment(f_actupeople, getString(R.string.tab_tendances));
        adapter.addFragment(f_stars, getString(R.string.tenue_decontractee));
        adapter.addFragment(f_telerealite, getString(R.string.tenue_pro_decontractee));
        adapter.addFragment(f_modebeaute, getString(R.string.tenue_ville));
        viewPager.setAdapter(adapter);
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setText("ACTU PEOPLE");
        tabLayout.getTabAt(1).setText("STARS");
        tabLayout.getTabAt(2).setText("TÉLÉ-RÉALITÉ");
        tabLayout.getTabAt(3).setText("MODE & BEAUTÉ");
    }

    private void initDrawerMenu() {
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerOpened(View drawerView) {
                hideKeyboard();
                updateSavedCounter(navigationView, R.id.nav_saved, App.getSaved().size());
                super.onDrawerOpened(drawerView);
            }
        };
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                menuItem.setChecked(true);
                displayFragment(menuItem.getItemId(), menuItem.getTitle().toString());
                drawer.closeDrawers();
                return true;
            }
        });
    }

    private void updateSavedCounter(NavigationView nav, @IdRes int itemId, int count) {
        TextView view = (TextView) nav.getMenu().findItem(itemId).getActionView().findViewById(R.id.counter);
        view.setText(String.valueOf(count));
    }

    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void displayFragment(int id, String title) {
        actionbar.setDisplayShowCustomEnabled(false);
        actionbar.setDisplayShowTitleEnabled(true);
        actionbar.setTitle(title);
        switch (id) {
            case R.id.nav_channel:
                Intent myIntent = new Intent(ActivityMain.this, ActivityFavori.class);
                ActivityMain.this.startActivity(myIntent);
                break;
            case R.id.nav_saved:
                Intent myIntent1 = new Intent(ActivityMain.this, ActivityFavori.class);
                ActivityMain.this.startActivity(myIntent1);
//                fragment = new FragmentSaved();
                break;
            case R.id.nav_setting:
                Intent myIntent2 = new Intent(ActivityMain.this, ActivityParametres.class);
                ActivityMain.this.startActivity(myIntent2);
//                fragment = new FragmentSetting();
                break;
        }
    }

    private void setupTabClick() {
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                viewPager.setCurrentItem(position);
                actionbar.setTitle(adapter.getTitle(position));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigationView.getMenu().getItem(0).setChecked(true);
    }
}
