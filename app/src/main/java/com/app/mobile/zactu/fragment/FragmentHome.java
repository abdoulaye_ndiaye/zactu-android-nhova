package com.app.mobile.zactu.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.mobile.zactu.ActivityMain;
import com.app.mobile.zactu.ActivityNewsDetails;
import com.app.mobile.zactu.BaseApp;
import com.app.mobile.zactu.R;
import com.app.mobile.zactu.adapter.AdapterNewsListWithHeader;
import com.app.mobile.zactu.adapter.NewsGridAdapter;
import com.app.mobile.zactu.data.Constant;
import com.app.mobile.zactu.data.Tools;
import com.app.mobile.zactu.model.News;
import com.app.mobile.zactu.model.PostResp;
import com.app.mobile.zactu.rest.PostService;
import com.app.mobile.zactu.tools.Utilities;
import com.app.mobile.zactu.view.EndlessRecyclerOnScrollListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

import java.util.List;
import java.util.Random;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class FragmentHome extends Fragment {

    private View root_view;
    private RecyclerView recyclerView;
    private ProgressBar progressbar;
    List<News> postItemList;
    TextView text_empry_list;
    ImageButton tryAgain;
    private int nextPage = 1;
    PostService postService = BaseApp.mobileAdapter.create(PostService.class);
    public static AdapterNewsListWithHeader mAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    private SearchView searchView;
    private boolean hasPage = true;
    private RewardedVideoAd mRewardedVideoAd;
    int randomNumber;
    Random random = new Random();
    Context context;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root_view = inflater.inflate(R.layout.fragment_home, null);
        context = getActivity();
        setHasOptionsMenu(true);

        recyclerView = (RecyclerView) root_view.findViewById(R.id.recyclerView);
        progressbar = (ProgressBar) root_view.findViewById(R.id.progressbar);
        text_empry_list = (TextView) root_view.findViewById(R.id.text_empry_list);
        tryAgain = (ImageButton) root_view.findViewById(R.id.try_again);
        swipeRefreshLayout = (SwipeRefreshLayout) root_view.findViewById(R.id.swipeRefreshLayout);
        initTabLayout();
        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(context);
        mRewardedVideoAd.setRewardedVideoAdListener(rewardedVideoAdListener);
        loadRewardedVideoAd();

        tryAgain.setOnClickListener(tryAgainListener);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                nextPage = 1;
                    getclientInfos(getCategory());
            }
        });

        return root_view;
    }

    RewardedVideoAdListener rewardedVideoAdListener = new RewardedVideoAdListener() {
        @Override
        public void onRewardedVideoAdLoaded() {
//            Toast.makeText(getActivity(), "onRewardedVideoAdLoaded", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRewardedVideoAdOpened() {
//            Toast.makeText(getActivity(), "onRewardedVideoAdOpened", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRewardedVideoStarted() {
//            Toast.makeText(getActivity(), "onRewardedVideoStarted", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRewardedVideoAdClosed() {
//            Toast.makeText(getActivity(), "onRewardedVideoAdClosed", Toast.LENGTH_SHORT).show();
            ////// UpdateDataBase
        }

        @Override
        public void onRewarded(RewardItem reward) {
//            Toast.makeText(getActivity(), getString(R.string.on_rewarded_video) + " " +  reward.getAmount() + " " + reward.getType(), Toast.LENGTH_LONG).show();
            // Reward the user.
        }

        @Override
        public void onRewardedVideoAdLeftApplication() {
//            Toast.makeText(getActivity(), "onRewardedVideoAdLeftApplication", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRewardedVideoAdFailedToLoad(int i) {
//            Toast.makeText(getActivity(), "onRewardedVideoAdFailedToLoad", Toast.LENGTH_SHORT).show();
        }
    };

    private void loadRewardedVideoAd() {
        mRewardedVideoAd.loadAd("ca-app-pub-6162942508854673/5457384669",
                new AdRequest.Builder().build());
    }

    private View.OnClickListener tryAgainListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            progressbar.setVisibility(View.VISIBLE);
            tryAgain.setVisibility(View.GONE);
            nextPage = 1;
            getclientInfos("");
        }
    };

    private void getclientInfos(String category) {
        if (!Utilities.isInternetAvailable(getActivity())) {
            progressbar.setVisibility(View.GONE);
            Utilities.showMessage(null, getString(R.string.no_internet), context);
            tryAgain.setVisibility(View.VISIBLE);
            return;
        }
        progressbar.setVisibility(View.VISIBLE);
        Call<PostResp> operations;
        operations = postService.postlist("", category, "", "", nextPage);
        operations.enqueue(callback);
    }

    private Callback<PostResp> page = new Callback<PostResp>() {
        @Override
        public void onResponse(Call<PostResp> call, Response<PostResp> res) {
            progressbar.setVisibility(View.VISIBLE);
            int code = res.code();
            if (code == 200) {
                postItemList.addAll(res.body().getPostItems());
                mAdapter.notifyDataSetChanged();

                progressbar.setVisibility(View.GONE);
            }
            if (code == 401) {
                ResponseBody base = res.errorBody();
                if (base != null) {
                    //TODO Client inconnu. Redirect to login screen
                    Toast.makeText(getActivity(), res.message(), Toast.LENGTH_SHORT)
                            .show();
                }
                getActivity().finish();
            }
            if (code == 500) {
                Toast.makeText(getActivity(), R.string.str_unknown_error, Toast.LENGTH_SHORT)
                        .show();
            }
            progressbar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<PostResp> call, Throwable t) {
            progressbar.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);
            tryAgain.setVisibility(View.VISIBLE);
            Toast.makeText(getActivity(), R.string.str_service_unavailable, Toast.LENGTH_SHORT)
                    .show();
        }
    };

    private Callback<PostResp> callback = new Callback<PostResp>() {

        @Override
        public void onResponse(Call<PostResp> call, Response<PostResp> res) {
            int code = res.code();
            if (code == 200) {
                postItemList = res.body().getPostItems();
                if (postItemList.isEmpty()){
                    text_empry_list.setVisibility(View.VISIBLE);
                }else {
                    text_empry_list.setVisibility(View.GONE);
                }
                initViews();
            } else if (code == 401) {
                ResponseBody base = res.errorBody();
                if (base != null) {
                    Toast.makeText(getActivity(), base.toString(), Toast.LENGTH_SHORT).show();
                }

                getActivity().finish();
            } else if (code == 500) {
                Toast.makeText(getActivity(), R.string.str_unknown_error, Toast.LENGTH_SHORT)
                        .show();
            } else {
                Utilities.showMessage(R.string.str_zactu, R.string.str_service_unavailable, context);
            }
            progressbar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<PostResp> call, Throwable t) {
            progressbar.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);
            tryAgain.setVisibility(View.VISIBLE);
            Toast.makeText(context, R.string.str_service_unavailable, Toast.LENGTH_SHORT)
                    .show();
        }
    };

    private void initTabLayout(){
        getclientInfos(getCategory());
    }

    private void initViews() {
        recyclerView.setVisibility(View.VISIBLE);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.scrollToPosition(0);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public boolean onLoadMore(int current_page) {
                if (!hasPage) {
                    return false;
                }
                if (!Utilities.isInternetAvailable(getActivity())) {
                    Toast.makeText(getActivity(), R.string.str_no_internet, Toast.LENGTH_SHORT)
                            .show();
                    tryAgain.setVisibility(View.VISIBLE);
                    return false;
                }
                progressbar.setVisibility(View.VISIBLE);
                Call<PostResp> operations = postService
                        .postlist("", "", "", "", current_page + 1);
                operations.enqueue(page);
                return true;
            }
        });

        if (postItemList.size() == 10) {
            nextPage++;
            hasPage = true;
        } else {
            hasPage = false;
        }
        if (postItemList.size() > 0){
            mAdapter = new AdapterNewsListWithHeader(getActivity(), postItemList.get(0), postItemList);
        }else {
            mAdapter = new AdapterNewsListWithHeader(getActivity(), null, postItemList);
        }
        recyclerView.setAdapter(mAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter.notifyDataSetChanged();

        mAdapter.setOnItemClickListener(new AdapterNewsListWithHeader.OnItemClickListener() {
            @Override
            public void onItemClick(View v, News obj, int position) {
                ActivityNewsDetails.navigate((ActivityMain)getActivity(), v.findViewById(R.id.image), obj);
            }
        });
        swipeRefreshLayout.setRefreshing(false);
        progressbar.setVisibility(View.GONE);
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setIconified(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                try {
                    mAdapter.getFilter().filter(s);
                } catch (Exception e) {
                }
                return true;
            }
        });
        // Detect SearchView icon clicks
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setItemsVisibility(menu, searchItem, false);
            }
        });

        // Detect SearchView close
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                setItemsVisibility(menu, searchItem, true);
                return false;
            }
        });
        searchView.onActionViewCollapsed();
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void setItemsVisibility(Menu menu, MenuItem exception, boolean visible) {
        for (int i = 0; i < menu.size(); ++i) {
            MenuItem item = menu.getItem(i);
            if (item != exception) item.setVisible(visible);
        }
    }

    public abstract String getCategory();

}
