package com.app.mobile.zactu.rest;

import com.app.mobile.zactu.model.ResponseBase;
import com.app.mobile.zactu.model.Client;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by ghambyte on 25/04/2017 at 17:26.
 * Project name : Modelic-V2
 */
public interface UserService {

    @POST("/postUser")
    Call<ResponseBase> postUser(@Body Client client);
}
