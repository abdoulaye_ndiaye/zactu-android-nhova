package com.app.mobile.zactu.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.mobile.zactu.R;
import com.app.mobile.zactu.model.PhotoModel;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by ghambyte on 23/11/2017 at 12:45.
 * Project name : BGFIMobile
 */

public class SliderAdapter extends PagerAdapter {

    private List<PhotoModel> images;
    private LayoutInflater inflater;
    private Context context;

    public SliderAdapter(Context context, List<PhotoModel> images) {
        this.context = context;
        this.images=images;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View myImageLayout = inflater.inflate(R.layout.slide, view, false);
        final ImageView myImage = (ImageView) myImageLayout
                .findViewById(R.id.image);
        final TextView textView1 = (TextView) myImageLayout
                .findViewById(R.id.textdetail);
        Picasso.with(myImage.getContext()).load(images.get(position).getUrl()).into(myImage);
        textView1.setText(images.get(position).getTitre());
        view.addView(myImageLayout, 0);
        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }
}
