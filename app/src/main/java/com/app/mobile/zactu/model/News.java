package com.app.mobile.zactu.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@IgnoreExtraProperties
public class News implements Serializable{
    private String title;
    private String more;
    private String date;
    private String url;
    private String id;
    private String posttitre;
    private String posttexte;
    private String datepost;
    private String category;
    List<PhotoModel> urls;
    List<VenteModel> ventes;
    private int image;
    private String content;
    private Channel channel;
    public int starCount = 0;
    public Map<String, Boolean> stars = new HashMap<>();

    public News() {
    }

    public News(int starCount) {
        this.starCount = starCount;
    }

    public int getStarCount() {
        return starCount;
    }

    public void setStarCount(int starCount) {
        this.starCount = starCount;
    }

    public News(String title, String date, int image, String content, Channel channel) {
        this.title = title;
        this.date = date;
        this.image = image;
        this.content = content;
        this.channel = channel;
    }

    public News(String title, String more, String url, String id, String date, int image, String content, Channel channel) {
        this.title = title;
        this.more = more;
        this.url = url;
        this.id = id;
        this.date = date;
        this.image = image;
        this.content = content;
        this.channel = channel;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMore() {
        return more;
    }

    public void setMore(String more) {
        this.more = more;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

//    public String getShort_content() {
//        if (content != null){
//            if(content.length()>100){
//                return content.substring(0, 80)+"...";
//            }
//            return content+"...";
//        }else {
//            return null;
//        }
//    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public List<PhotoModel> getUrls() {
        return urls;
    }

    public void setUrls(List<PhotoModel> urls) {
        this.urls = urls;
    }

    public String getPosttitre() {
        return posttitre;
    }

    public void setPosttitre(String posttitre) {
        this.posttitre = posttitre;
    }

    public String getPosttexte() {
        return posttexte;
    }

    public void setPosttexte(String posttexte) {
        this.posttexte = posttexte;
    }

    public String getDatepost() {
        return datepost;
    }

    public void setDatepost(String datepost) {
        this.datepost = datepost;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<VenteModel> getVentes() {
        return ventes;
    }

    public void setVentes(List<VenteModel> ventes) {
        this.ventes = ventes;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("id", id);
        result.put("title", title);
        result.put("more", more);
        result.put("date", date);
        result.put("url", url);
        result.put("posttitre", posttitre);
        result.put("posttexte", posttexte);
        result.put("datepost", datepost);
        result.put("category", category);
        result.put("urls", urls);
        result.put("ventes", ventes);
        result.put("starCount", starCount);
        result.put("stars", stars);
        result.put("content", content);
        result.put("stars", stars);

        return result;
    }
}
